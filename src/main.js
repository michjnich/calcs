// @ts-nocheck
import { createApp } from 'vue'
import ElementPlus from 'element-plus'

import App from './App.vue'
import router from "@/router/index.js"
import { store } from "@/store/index.js"

import 'element-plus/lib/theme-chalk/index.css'
import '@/assets/css/main.css'

const app = createApp(App)  

app
  .use(ElementPlus)
  .use(router)
  .use(store)
  .mount('#app')
