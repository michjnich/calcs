import { createWebHistory, createRouter } from 'vue-router'

// Defaults
import Home from '@/views/Home.vue'
import About from '@/views/About.vue'
import NotFound from '@/views/NotFound.vue'
// import NotImplemented from '@/views/NotImplemented'
// Health
import BMI from '@/views/health/BMI.vue'
import BodyFat from '@/views/health/BodyFat.vue'
// Maths
import Percentage from '@/views/maths/Percentage.vue'
import Bitwise from '@/views/maths/Bitwise.vue'
// Conversions
import Conversion from '@/views/conversions/Conversion.vue'


const routes = [
    { path: '/', name: "Home", component: Home },
    { path: '/about', name: "About", component: About },
    // Health
    { path: '/bmi', name: "BMI", component: BMI },
    { path: '/bodyfat', name: "BodyFat", component: BodyFat },
    // Maths
    { path: '/percent', name: "Percentage", component: Percentage },
    { path: '/bitwise', name: "Bitwise", component: Bitwise },
    // Conversions
    { path: '/conversion', name: "Conversion", component: Conversion },
    { path: '/:catchAll(.*)', name: "404", component: NotFound }
];

const router = createRouter({
    history: createWebHistory(),
    routes
    // shorthand routes:routes
});
export default router;