import { createStore } from 'vuex'

export const store = createStore({
    state: {    
        decimalPlaces: 4,
    },
    mutations: {
        changeDecimalPlaces: (state, newDecPlaces) => {
            state.decimalPlaces = newDecPlaces
        }
    }
})
