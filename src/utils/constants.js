export const DISTANCE_UNITS = {
    // Metric
    Millimeter: { abbr: "mm", conv: 0.001 },
    Centimeter: { abbr: "cm", conv: 0.01 },
    Decimeter: { abbr: "dm", conv: 0.1 },
    Meter: { abbr: "m", conv: 1 },
    Kilometer: { abbr: "km", conv: 1000 },
    // Imperial
    Inch: { abbr: '"', conv: 0.0254 },
    Foot: { abbr: "'", conv: 0.3048 },
    Yard: { abbr: "yd", conv: 0.9144 },
    Mile: { abbr: "mi", conv: 1609.34 },
    // Nautical
    Fathom: { abbr: "fm", conv: 1.8288 },
    NauticalMile: { abbr: "nm", conv: 1852 },
    // US surveying
    Chain: { abbr: "ch", conv: 20.1168 },
    Rod: { abbr: "P", conv: 5.0292 },
  }


  export const WEIGHT_UNITS = {
    //Metric
    Milligram: { abbr: "mg", conv: 0.000001 },
    Centiigram: { abbr: "cg", conv: 0.00001 },
    Decigram: { abbr: "dg", conv: 0.0001 },
    Gram: { abbr: "g", conv: 0.001 },
    Dekagram: { abbr: "dag", conv: 0.01 },
    Hectogram: { abbr: "hg", conv: 0.1 },
    Kilogram: { abbr: "kg", conv: 1 },
    MetricTon: { abbr: "mtu", conv: 1000},
    //Imperial
    Grain: { abbr: "gr", conv: 0.00006479891},
    Dram: { abbr: "dr", conv: 0.0017718451953125},
    Ounce: { abbr: "oz", conv: 0.02834952},
    Pound: { abbr: "lbs", conv: 0.45359237},
    // 1 short hundredweight = 100 pounds
    // 1 long hundredweight = 112 pounds
    // 1 short ton = 2,000 pounds
    // 1 long ton = 2,240 pounds
    // Apothecary 
    // 1 grain = 1/7,000 avoirdupois pound = 1/5,760 troy or apothecaries' pound
    // 1 apothecaries' scruple = 20 grains = 1/3 dram
    // 1 pennyweight = 24 grains = 1/20 troy ounce
    // 1 apothecaries' dram = 60 grains = 1/8 apothecaries' ounce
    // 1 troy or apothecaries' ounce = 480 grains = 1/12 troy or apothecaries' pound
    // 1 troy or apothecaries' pound = 5,760 grains = 5,760/7,000 avoirdupois pound

  }