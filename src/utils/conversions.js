const POUNDS_IN_KG = 2.20462262185;
const CM_IN_INCH = 2.54;



// Length 
export function cmToInches(cm) {
    return Math.round(cm / CM_IN_INCH);
}

export function cmToWholeFeet(cm) {
    return Math.floor(cmToInches(cm) / 12);
}

export function cmToWholeInches(cm) {
    return cmToInches(cm) % 12
}

export function inchesToFeetAndInches(inches) {
    const feet = Math.floor(inches / 12);
    const remainderInches  = inches % 12;
    return { "feet": feet, "inches": remainderInches }
}

export function inchesToCm(inches) {
    return Math.round(inches * CM_IN_INCH)
}

export function feetAndInchesToCm(feet, inches) {
    return inchesToCm((feet * 12) + inches)
}

// Weight 
export function kgToLbs(kg) {
    return Math.round(kg * POUNDS_IN_KG)
}

export function kgToWholeStone(kg) {
    return Math.floor(kgToLbs(kg) / 14)
}

export function kgToWholeLbs(kg) {
    return kgToLbs(kg) % 14
}

export function stoneAndLbsToKg(stone, lbs) {
    return Math.round(((stone * 14) + lbs) / POUNDS_IN_KG);
}

// Maths

export function dec2bin(dec) {
    return (dec >>> 0).toString(2)
}