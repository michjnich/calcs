export function optionList(optionDict) {
    var options = []
    for (const [opt, sym] of Object.entries(optionDict)) {
        options.push({ label: opt, desc: sym })
    }
    return options
}