# calcs
[![Netlify Status](https://api.netlify.com/api/v1/badges/4e9f5452-35ad-4f08-93bf-df9e5355cdf0/deploy-status)](https://app.netlify.com/sites/michjnich-calcs/deploys)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
